
class TestaContas{
	public static void main(String[] args){
		Conta novaConta = new ContaCorrente();
		ContaCorrente novaCC = new ContaCorrente();
		ContaPoupanca novaCP = new ContaPoupanca();

		novaConta.deposita(1000);
		novaCC.deposita(1000);
		novaCP.deposita(1000);

		novaConta.atualiza(0.01);		
		novaCC.atualiza(0.01);
		novaCP.atualiza(0.01);

		System.out.println(novaConta.getSaldo());
		System.out.println(novaCC.getSaldo());
		System.out.println(novaCP.getSaldo());
	}
}
